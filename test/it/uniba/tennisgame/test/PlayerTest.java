package it.uniba.tennisgame.test;

import static org.junit.Assert.*;

import org.junit.Test;

import it.uniba.tennisgame.Player;

public class PlayerTest {

	@Test
	public void scoreShoudBeIncreased() {
		// Arrange
		Player player = new Player("Federer", 0);
		
		// Act
		player.incrementScore();
		
		// Assert
		assertEquals(1, player.getScore());
	}
	
	@Test
	public void scoreShoudNotBeIncreased() {
		// Arrange
		Player player = new Player("Federer", 0);
		
		// Act
		
		// Assert
		assertEquals(0, player.getScore());
	}
	
	@Test
	public void scoreShoudBeLove() {
		// Arrange
		Player player = new Player("Federer", 0);
		
		// Act
		String scoreAsString = player.getScoreAsString();
		
		// Assert
		assertEquals("love", scoreAsString);
	}
	
	@Test
	public void scoreShoudBeFifteen() {
		// Arrange
		Player player = new Player("Federer", 1);
		
		// Act
		String scoreAsString = player.getScoreAsString();
		
		// Assert
		assertEquals("fifteen", scoreAsString);
	}
	
	@Test
	public void scoreShoudBeThirty() {
		// Arrange
		Player player = new Player("Federer", 2);
		
		// Act
		String scoreAsString = player.getScoreAsString();
		
		// Assert
		assertEquals("thirty", scoreAsString);
	}
	
	@Test
	public void scoreShoudBeForty() {
		// Arrange
		Player player = new Player("Federer", 3);
		
		// Act
		String scoreAsString = player.getScoreAsString();
		
		// Assert
		assertEquals("forty", scoreAsString);
	}
	
	@Test
	public void scoreShouldBeNullIfNegative() {
		// Arrange
		Player player = new Player("Federer", -1);
		
		// Act
		String scoreAsString = player.getScoreAsString();
		
		// Assert
		assertNull(scoreAsString);
	}
	
	@Test
	public void scoreShoudBeNullIfMoreThanThree() {
		// Arrange
		Player player = new Player("Federer", 4);
		
		// Act
		String scoreAsString = player.getScoreAsString();
		
		// Assert
		assertNull(scoreAsString);
	}
	
	@Test
	public void scoreShouldBeTie() {
		// Arrange
		Player player1 = new Player("Federer", 0);
		Player player2 = new Player("Nadal", 0);
		
		// Act
		boolean tie = player1.isTieWith(player2);
		
		// Assert
		assertTrue(tie);
	}
	
	@Test
	public void scoreShouldNotBeTie() {
		// Arrange
		Player player1 = new Player("Federer", 0);
		Player player2 = new Player("Nadal", 1);
		
		// Act
		boolean tie = player1.isTieWith(player2);
		
		// Assert
		assertFalse(tie);
	}
	
	@Test
	public void scoreShouldBeAtLeastForty() {
		// Arrange
		Player player = new Player("Federer", 3);
		
		// Act
		boolean atLeastFortyPoints = player.hasAtLeastFortyPoints(); 
		
		// Assert
		assertTrue(atLeastFortyPoints);
	}
	
	@Test
	public void scoreShouldNotBeAtLeastForty() {
		// Arrange
		Player player = new Player("Federer", 2);
		
		// Act
		boolean atLeastFortyPoints = player.hasAtLeastFortyPoints(); 
		
		// Assert
		assertFalse(atLeastFortyPoints);
	}
	
	@Test
	public void scoreShouldBeLessThanForty() {
		// Arrange
		Player player = new Player("Federer", 2);
		
		// Act
		boolean lessThanFortyPoints = player.hasLessThanFortyPoints(); 
		
		// Assert
		assertTrue(lessThanFortyPoints);
	}
	
	@Test
	public void scoreShouldNotBeLessThanForty() {
		// Arrange
		Player player = new Player("Federer", 3);
		
		// Act
		boolean lessThanFortyPoints = player.hasLessThanFortyPoints(); 
		
		// Assert
		assertFalse(lessThanFortyPoints);
	}
	
	@Test
	public void scoreShouldBeMoreThanForty() {
		// Arrange
		Player player = new Player("Federer", 4);
		
		// Act
		boolean moreThanFortyPoints = player.hasMoreThanFourtyPoints(); 
		
		// Assert
		assertTrue(moreThanFortyPoints);
	}
	
	@Test
	public void scoreShouldNotBeMoreThanForty() {
		// Arrange
		Player player = new Player("Federer", 3);
		
		// Act
		boolean moreThanFortyPoints = player.hasMoreThanFourtyPoints(); 
		
		// Assert
		assertFalse(moreThanFortyPoints);
	}
	
	@Test
	public void shouldHaveOnePointAdvantageOn() {
		// Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 2);
		
		// Act
		boolean onePointAdvantage = player1.hasOnePointAdvantageOn(player2); 
		
		// Assert
		assertTrue(onePointAdvantage);
	}
	
	@Test
	public void shouldNotHaveOnePointAdvantageOn() {
		// Arrange
		Player player1 = new Player("Federer", 2);
		Player player2 = new Player("Nadal", 2);
		
		// Act
		boolean onePointAdvantage = player1.hasOnePointAdvantageOn(player2); 
		
		// Assert
		assertFalse(onePointAdvantage);
	}
	
	@Test
	public void shouldHaveAtLeastTwoPointsAdvantageOn() {
		// Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 1);
		
		// Act
		boolean twoPointsAdvantage = player1.hasAtLeastTwoPointsAdvantageOn(player2); 
		
		// Assert
		assertTrue(twoPointsAdvantage);
	}

	@Test
	public void shouldNotHaveAtLeastTwoPointsAdvantageOn() {
		// Arrange
		Player player1 = new Player("Federer", 2);
		Player player2 = new Player("Nadal", 1);
		
		// Act
		boolean twoPointsAdvantage = player1.hasAtLeastTwoPointsAdvantageOn(player2); 
		
		// Assert
		assertFalse(twoPointsAdvantage);
	}
}