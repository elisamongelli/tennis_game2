package it.uniba.tennisgame.test;

import static org.junit.Assert.*;

import org.junit.Test;

import it.uniba.tennisgame.DuplicatedPlayerException;
import it.uniba.tennisgame.Game;


// FILE EXCEL CON STRONG NORMAL EQUIVALENCE CLASS TESTING

public class GameTest {
	
	@Test
	public void testFifteenThirty() throws Exception {
		// Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		// Act - vedi TC1 nel foglio excel
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		// Assert
		assertEquals("Federer fifteen - Nadal thirty", status);
	}
	
	@Test
	public void testFortyThirty() throws Exception {
		// Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		// Act - vedi TC2 nel foglio excel
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		// Assert
		assertEquals("Federer forty - Nadal thirty", status);
	}
	
	@Test
	public void testPlayer1Wins() throws Exception {
		// Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		// Act - vedi TC3 nel foglio excel
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		
		String status = game.getGameStatus();
		
		// Assert
		assertEquals("Federer wins", status);
	}
	
	@Test
	public void testFifteenForty() throws Exception {
		// Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		// Act - vedi TC4 nel foglio excel
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		// Assert
		assertEquals("Federer fifteen - Nadal forty", status);
	}
	
	@Test
	public void testDeuceFortyForty() throws Exception {
		// Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		// Act - vedi TC5 nel foglio excel
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		// Assert
		assertEquals("Deuce", status);
	}
	
	@Test
	public void testAdvantagePlayer1() throws Exception {
		// Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		// Act - vedi TC6 nel foglio excel
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		
		String status = game.getGameStatus();
		
		// Assert
		assertEquals("Advantage Federer", status);
	}
	
	@Test
	public void testPlayer2Wins() throws Exception {
		// Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		// Act - vedi TC7 nel foglio excel
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		// Assert
		assertEquals("Nadal wins", status);
	}
	
	@Test
	public void testAdvantagePlayer2() throws Exception {
		// Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		// Act - vedi TC8 nel foglio excel
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		// Assert
		assertEquals("Advantage Nadal", status);
	}

	@Test
	public void testDeuceBothMoreThanForty() throws Exception {
		// Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		// Act - vedi TC9 nel foglio excel
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		
		String status = game.getGameStatus();
		
		// Assert
		assertEquals("Deuce", status);
	}
}
